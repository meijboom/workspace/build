#!/bin/bash

mkdir -p output

docker build -t workspace-kernel-build . && \
    id=$(docker create workspace-kernel-build) && \
    docker cp $id:/build/vmlinux output/vmlinux && \
    docker rm -f $id
