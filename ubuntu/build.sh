#!/bin/bash

set -e

# load preset
export PRESET=eoan
eval $(cat presets/$PRESET.sh)

# create output directory
mkdir -p output

# make sure everything is unmounted on exit
function unmount() {
    local mounts_left=0

    for name in $(mount | grep /opt/ubuntu | awk '{print $3}')
    do
        umount $name 2>/dev/null || true
        mounts_left=1
        sleep 1
    done

    if [ $mounts_left -eq 1 ]; then
        unmount
    fi
}

trap unmount EXIT

# create rootfs
if [ ! -f "output/ubuntu.img" ]; then
    echo ">> create filesystem"
    mkdir -p /opt/ubuntu && \
	fallocate -l 10G output/ubuntu.img && \
        mkfs.ext4 output/ubuntu.img && \
        mount output/ubuntu.img /opt/ubuntu && \
        debootstrap --arch=amd64 --variant=buildd $UBUNTU_VERSION /opt/ubuntu http://archive.ubuntu.com/ubuntu/
else
    mount output/ubuntu.img /opt/ubuntu
fi

# setup ubuntu
echo ">> setup ubuntu base"
mount --bind /dev /opt/ubuntu/dev && \
    mount --bind /proc /opt/ubuntu/proc && \
    mount --bind /sys /opt/ubuntu/sys && \
    mkdir -p /opt/ubuntu/output && \
    mount --bind output /opt/ubuntu/output && \
    cp setup.sh /opt/ubuntu/setup.sh && \
    cp presets/$PRESET.sh /opt/ubuntu/preset.sh && \
    chroot /opt/ubuntu /setup.sh
