#!/bin/bash

set -e

echo ">> set environment"
export TERM=xterm
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin
export DEBIAN_FRONTEND=noninteractive
unset XDG_DATA_HOME
unset XDG_DATA_DIRS

echo ">> load preset"
eval $(cat /preset.sh)

echo ">> configure system"
echo "nameserver $PRIMARY_NAMESERVER" > /etc/resolv.conf && \
    echo "
deb http://nl.archive.ubuntu.com/ubuntu/ ${UBUNTU_VERSION} main restricted universe multiverse
deb http://nl.archive.ubuntu.com/ubuntu/ ${UBUNTU_VERSION}-security main restricted universe multiverse
deb http://nl.archive.ubuntu.com/ubuntu/ ${UBUNTU_VERSION}-updates main restricted universe multiverse
deb http://nl.archive.ubuntu.com/ubuntu/ ${UBUNTU_VERSION}-backports main restricted universe multiverse
    " > /etc/apt/sources.list

echo ">> installing packages"
apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y btrfs-progs ubuntu-minimal linux-generic linux-image-generic linux-headers-generic language-pack-en-base grub-efi-amd64 curl dpkg initramfs-tools ${PACKAGES}

echo ">> installing bootloader"
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=grub && \
    mkdir /boot/EFI/boot && \
    cp /boot/EFI/grub/grubx64.efi /boot/EFI/boot/bootx64.efi

echo ">> extracting kernel image"
curl -LO https://raw.githubusercontent.com/torvalds/linux/master/scripts/extract-vmlinux && \
    sh extract-vmlinux /boot/vmlinuz > /output/vmlinux && \
    rm extract-vmlinux

echo ">> extracting initramfs"
cp /boot/initrd.img /output/initrd.img

echo ">> creating user"
echo "root:root" | chpasswd
