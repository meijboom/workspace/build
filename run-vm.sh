#!/bin/bash

set -e

crosvm run -s crosvm.sock --rwroot ubuntu/output/ubuntu.img -m 2048 -i output/initrd.img  output/vmlinux
